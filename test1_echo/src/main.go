package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"

	models "./models"
)

func main() {
	// Echoのインスタンス作る
	e := echo.New()

	// 全てのリクエストで差し込みたいミドルウェア（ログとか）はここ
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// ルーティング
	e.Get("/animals", models.GetAnimals())
	e.Get("/animals/:id", models.GetAnimal())
	e.Post("/animals", models.CreateAnimal())
	e.Put("/animals/:id", models.UpdateAnimal())

	// DB初期化
	models.Init()

	// サーバー起動
	e.Run(standard.New(":1323")) //ポート番号指定してね
}
