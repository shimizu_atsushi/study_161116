package models

import (
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Animal Json define
type Animal struct {
	ID             int32
	Name           string
	Classification string
}

func Init() {
	// Migrate the schema
	db, err := gorm.Open("postgres", "user=shimizu dbname=test password=june-cle")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	db.AutoMigrate(&Animal{})

	// Create
	db.Create(&Animal{ID: 1, Name: "犬", Classification: "哺乳類"})
	db.Create(&Animal{ID: 2, Name: "猫", Classification: "哺乳類"})
	db.Create(&Animal{ID: 3, Name: "鶏", Classification: "鳥類"})
	db.Create(&Animal{ID: 4, Name: "トカゲ", Classification: "爬虫類"})
	db.Create(&Animal{ID: 5, Name: "蛙", Classification: "両生類"})
	db.Create(&Animal{ID: 6, Name: "魚", Classification: "魚類"})
}

func GetAnimals() echo.HandlerFunc {
	return func(c echo.Context) error {
		db, err := gorm.Open("postgres", "user=shimizu dbname=test password=june-cle")
		if err != nil {
			panic("failed to connect database")
		}
		defer db.Close()
		animals := []Animal{}
		db.Find(&animals)
		return c.JSON(http.StatusOK, animals)
	}
}

func GetAnimal() echo.HandlerFunc {
	return func(c echo.Context) error {
		db, err := gorm.Open("postgres", "user=shimizu dbname=test password=june-cle")
		if err != nil {
			panic("failed to connect database")
		}
		defer db.Close()
		id, parseerr := strconv.Atoi(c.Param("id"))
		if parseerr != nil {
			panic("failed to connect database")
		}
		animal := Animal{}
		db.First(&animal, id)
		// return c.JSON(http.StatusOK, animal)
		return c.String(http.StatusOK, animal.Name)
	}
}

func CreateAnimal() echo.HandlerFunc {
	return func(c echo.Context) error {
		db, err := gorm.Open("postgres", "user=shimizu dbname=test password=june-cle")
		if err != nil {
			panic("failed to connect database")
		}
		defer db.Close()
		animal := Animal{}
		lastAnimal := Animal{}
		db.Last(&lastAnimal)
		animal.ID = lastAnimal.ID + 1
		animal.Name = c.FormValue("name")
		animal.Classification = c.FormValue("classification")
		db.Create(&Animal{ID: animal.ID, Name: animal.Name, Classification: animal.Classification})
		return c.JSON(http.StatusCreated, animal)
	}
}

func UpdateAnimal() echo.HandlerFunc {
	return func(c echo.Context) error {
		db, err := gorm.Open("postgres", "user=shimizu dbname=test password=june-cle")
		if err != nil {
			panic("failed to connect database")
		}
		defer db.Close()
		id, parseerr := strconv.Atoi(c.Param("id"))
		if parseerr != nil {
			panic("failed to connect database")
		}
		animal := Animal{}
		db.First(&animal, id)
		animal.Name = c.FormValue("name")
		animal.Classification = c.FormValue("classification")
		db.Save(&animal)
		return c.JSON(http.StatusNoContent, animal)
	}
}
